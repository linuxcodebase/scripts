$path = "C:\log\trace" # Папка хранения логов
$workpath = "C:\log\" # Папка с временными .txt файлами
$period = "-7" # Количество дней хранения логов.

# Задаем имя папки и файла в cконвертированное в GMT +3 локальное время компьютера
$dirName = [System.TimeZoneInfo]::ConvertTimeBySystemTimeZoneId((Get-Date), "Russian Standard Time")
$dirName = $dirName.ToString("ddMMyyyy")
$fileName = [System.TimeZoneInfo]::ConvertTimeBySystemTimeZoneId((Get-Date), "Russian Standard Time")
$fileName = $filename.ToString("HHmmss")

# Вычисляем дату после которой будем удалять файлы.
$currentDay = Get-Date
$chDaysDel = $currentDay.AddDays($period)

# Удаление файлов, дата создания которых больше заданного количества дней
#Get-ChildItem -Path "$path" -Recurse | Where-Object {$_.CreationTime -lt $chDaysDel} | Remove-Item -Recurse -Force 
 
# Удаление пустых папок
#Get-ChildItem -Path "$path" -Recurse | Where-Object {$_.PSIsContainer -and @(Get-ChildItem -Path $_.Fullname -Recurse `
#                                                                  | Where { -not $_.PSIsContainer }).Count -eq 0 } | Remove-Item -Recurse

# Проверка наличия папки $workpath
if (-not (test-path -Path $workpath)) {New-Item -Path "C:\" -Name log -Type directory}

# Проверка наличия папки $path
if (-not (test-path -Path $path)) {New-Item -Path $workpath -Name trace -Type directory}

# Проверка наличия папки $dirName
if (-not (test-path -Path "$path\$dirName")) {New-Item -Path $path -Name $dirName -Type directory}

# Проверка наличия файла tracert.bat
if (-not (test-path -Path $workpath\tracert.bat)) {New-Item -Name "tracert.bat" -Path $workpath -ItemType "file"}



# Добавляем необходимые хосты для трассировки в tracert.bat;
# Хосты для трассировки необходимо добавлять в Add-Content -Value ниже.
Clear-Content -Path "$workpath\tracert.bat"
Add-Content -Path "$workpath\tracert.bat" -Value "start /b tracert -4 yandex.ru 1> C:\log\s01.txt",`
                                                 "start /b tracert -4 habr.com/ 1> C:\log\s02.txt",`
                                                 "start /b tracert -4 cloudflare.com 1> C:\log\s03.txt",`
                                                 "start /b tracert -4 1.1.1.1 1> C:\log\one.txt";

# Запускаем tracert.bat и передаем информацию о нем в переменную $ParentID
$parentID = (Start-Process "cmd.exe" "/c $workpath\tracert.bat" -WindowStyle Hidden -PassThru)

# Спим секунду, чтобы дочерние процессы успели получить свои PID
Start-Sleep -s 1

# Получаем PID дочерних процессов в $ChildID (исключая conhost.exe)
$childID = Get-WmiObject Win32_Process | Select ProcessID, ParentProcessID, Description | Select-String $parentID.Id | Select-String "tracert.exe"

# Приостанавливаем скрипт, пока работают трассировки
while ($childID -ne $null) {
      Start-Sleep -s 1
      $childID = Get-WmiObject Win32_Process | Select ProcessID, ParentProcessID, Description | Select-String $parentID.Id | Select-String "tracert.exe"
}
Get-Content "$workpath\s01.txt", "$workpath\s02.txt", "$workpath\s03.txt", "$workpath\one.txt" | Out-File "$path\$dirName\$fileName.txt" -Append -Encoding ascii